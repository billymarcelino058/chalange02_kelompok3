package service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.DataSekolah;
import repository.DataSekolahRepository;

/***
 * Operation for DataSekolah
 */
public class DataSekolahService implements DataSekolahRepository {
    private static final String newLine = "\n";
    private static final List<Integer> number = DataSekolah.read();

    /***
     * Group the data from DataSekolah
     * 
     * @returns the Map with Key as value in DataSekolah
     *          and Value as the amount of it.
     */
    public Map<Integer, Integer> groupAll() {
        List<Integer> numbers = DataSekolah.read();
        Map<Integer, Integer> res = new LinkedHashMap<>();
        // int size = numbers.size();

        for (int nilai : numbers) {
            if (!res.keySet().contains(nilai)) {
                res.put(nilai, 1);
            } else {
                int lastValue = res.get(nilai);
                res.replace(nilai, ++lastValue);
            }
        }
        return res;
    }

    /***
     * Group the data from DataSekolah with Key less than value given in param
     * 
     * @param input is the number given as the standard score
     * @returns the Map with Key as value in DataSekolah
     *          and Value as the amount of it.
     */
    public Map<Integer, Integer> showLessThan(int input) {
        Map<Integer, Integer> data = groupAll();
        Map<Integer, Integer> res = new LinkedHashMap<>();
        Set<Map.Entry<Integer, Integer>> entries = data.entrySet();

        for (Map.Entry<Integer, Integer> entry : entries) {
            int key = entry.getKey();
            int value = entry.getValue();
            if (key < input) {
                res.put(key, value);
            }
        }
        return res;
    }

    /***
     * Parse the Map from showLessThan() to String with StringBuffer
     * 
     * @param input is the number given as the standard score
     * @returns String contains keys and values
     *          from Map in showLessThan().
     */
    public String writeLessThan(int input) {
        Set<Map.Entry<Integer, Integer>> entries = showLessThan(input).entrySet();
        String res = "";
        StringBuffer sbr = new StringBuffer();

        for (Map.Entry<Integer, Integer> entry : entries) {
            int key = entry.getKey();
            int value = entry.getValue();
            sbr.append("Nilai ");
            sbr.append(key);
            sbr.append(" berjumlah ");
            sbr.append(value);
            sbr.append(newLine);
        }
        res = sbr.toString();
        return res;
    }

    /***
     * Group the data from DataSekolah with Key more than value given in param
     * 
     * @param input is the number given as the standard score
     * @returns the Map with Key as value in DataSekolah
     *          and Value as the amount of it.
     */
    public Map<Integer, Integer> showMoreThan(int input) {
        Map<Integer, Integer> data = groupAll();
        Map<Integer, Integer> res = new LinkedHashMap<>();
        Set<Map.Entry<Integer, Integer>> entries = data.entrySet();

        for (Map.Entry<Integer, Integer> entry : entries) {
            int key = entry.getKey();
            int value = entry.getValue();
            if (key > input) {
                res.put(key, value);
            }
        }
        return res;
    }

    /***
     * Parse the Map from showMoreThan() to String with StringBuffer
     * 
     * @param input is the number given as the standard score
     * @returns String contains keys and values
     *          from Map in showMoreThan().
     */
    public String writeMoreThan(int input) {
        Set<Map.Entry<Integer, Integer>> entries = showMoreThan(input).entrySet();
        String res = "";
        StringBuffer sbr = new StringBuffer();

        for (Map.Entry<Integer, Integer> entry : entries) {
            int key = entry.getKey();
            int value = entry.getValue();
            sbr.append("Nilai ");
            sbr.append(key);
            sbr.append(" berjumlah ");
            sbr.append(value);
            sbr.append(newLine);
        }
        res = sbr.toString();
        return res;
    }

    /***
     * Count the value in Key input
     * 
     * @param input is the number given as the standard score
     * @returns String contains number of amount
     *          of number from the input.
     */
    public String getValueOf(int input) {
        String res = Integer.toString(groupAll().get(input));

        return res;
    }

    /***
     * Calculate the mean of numbers in List DataSekolah
     * 
     * @returns Double of the mean result
     */
    public double calcMean() {
        float sum = 0;
        float size = number.size();

        for (int i = 0; i < number.size(); i++) {
            sum += number.get(i);
        }
        double res = sum / size;
        return res;
    }

    /***
     * Calculate the mode of numbers in List DataSekolah
     * 
     * @returns Double of the mode result
     */
    public int calcMode() {
        int maxValue = 0, maxCount = 0, i, j;

        for (i = 0; i < number.size(); ++i) {
            int count = 0;
            for (j = 0; j < number.size(); ++j) {
                if (number.get(j) == number.get(i))
                    ++count;
            }

            if (count > maxCount) {
                maxCount = count;
                maxValue = number.get(i);
            }
        }
        return maxValue;
    }

    /***
     * Calculate the median of numbers in List DataSekolah
     * 
     * @returns Double of the median result
     */
    public double calcMedian() {
        double res = 0;

        if (number.size() % 2 == 1) {
            res = number.get(number.size() / 2);
        } else {
            res = ((double) (number.get(number.size() / 2) + number.get((number.size() / 2) - 1))) / 2;
        }
        return res;
    }

}
